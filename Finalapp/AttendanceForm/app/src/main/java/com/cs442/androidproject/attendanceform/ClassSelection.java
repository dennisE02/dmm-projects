package com.cs442.androidproject.attendanceform;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.cs442.androidproject.attendanceform.Model.Classes;
import com.cs442.androidproject.attendanceform.Model.StudentClasses;
import com.cs442.androidproject.attendanceform.Model.Users;
import com.kinvey.android.AsyncAppData;
import com.kinvey.android.Client;
import com.kinvey.android.callback.KinveyListCallback;
import com.kinvey.java.Query;

import java.util.ArrayList;

public class ClassSelection extends AppCompatActivity {
    private Client mAttendanceFormClient;
    final static String LOGTAG = "ATF";
    protected Button btadd;
    protected ListView list;
    Users cUser =  new Users();
    Classes[] allclass ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.prof_class_screen);
        mAttendanceFormClient = ((AttendanceFormApplication)getApplication()).getmAttendanceFormClient();
        btadd = (Button)findViewById(R.id.btnAddClass);
        btadd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //TODO (DONE)navigate to add class screen
                Intent toadd = new Intent(ClassSelection.this, ProfessorAddClass.class);
                startActivity(toadd);
            }
        });
        list = (ListView) findViewById(R.id.classList);
    }

    @Override
    protected void onResume() {
        super.onResume();

        cUser = MainActivity.getCurrentUser();

        if(cUser.getStatus_Flag().equals("P")){
            findAllProfessorClasses();
        }
        if(cUser.getStatus_Flag().equals("S")){
            btadd.setVisibility(View.INVISIBLE);
            findAllStudentClasses();
        }
    }

    private void findAllProfessorClasses(){


        String profemail = cUser.getUsersEmail();
        Query q = mAttendanceFormClient.query();
        q.equals("course_lecturer", profemail);
        Log.d(LOGTAG, "course code " + cUser.getUsersEmail());

        final AsyncAppData<Classes> myData = mAttendanceFormClient.appData("Classes", Classes.class);
        myData.get(q, new KinveyListCallback<Classes>() {
            @Override
            public void onFailure(Throwable t) {
                Log.d(LOGTAG, "Error fetching data: " + t.getMessage());
                return;
            }
            @Override
            public void onSuccess(Classes[] getClasses) {
                allclass = getClasses;
                Log.d(LOGTAG, "Classes fetched is " + getClasses.length);
                String[]coursename = new String[getClasses.length];
                String[] clecture= new String[getClasses.length];
                String [] day=new String[getClasses.length];
                Integer[] courseCode = new Integer[getClasses.length];

                ArrayList<String> combineStudent = new ArrayList<String>();
                for(int i=0; i<getClasses.length; i++) {
                    coursename[i] = getClasses[i].getCourse_name();
                    courseCode[i] = getClasses[i].getCourse_code();
                    clecture[i] =getClasses[i].getCourse_lecturer();
                    day[i]=getClasses[i].getCourse_days();
                    combineStudent.add( courseCode[i]+"   "+coursename[i]+"/n"+clecture[i]+" "+day[i]);
                }

                ArrayAdapter<String> adp=new ArrayAdapter<String>(ClassSelection.this, android.R.layout.simple_list_item_1,coursename);
                list.setAdapter(adp);
                list.setOnItemClickListener(
                        new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                alartforprofessor(position);
                            }
                        });
            }
        });
    }

    private void findAllStudentClasses(){

        Query q = mAttendanceFormClient.query();
        q.equals("usersEmail", cUser.getUsersEmail());

        AsyncAppData<StudentClasses> myData = mAttendanceFormClient.appData("StudentClasses", StudentClasses.class);
        myData.get(q, new KinveyListCallback<StudentClasses>() {
            @Override
            public void onFailure(Throwable t) {
                Log.d(LOGTAG, "Error fetching data: " + t.getMessage());
                return;
            }
            @Override
            public void onSuccess(final StudentClasses[] studentClasses) {
                Log.d(LOGTAG, "Classes fetched is " + studentClasses.length);
                allclass = new Classes[studentClasses.length];
                for(int i =0; i < studentClasses.length; i++) {
                    //fetch class details
                    Query q = mAttendanceFormClient.query();
                    q.equals("course_code", studentClasses[i].getCourse_code());
                    final AsyncAppData<Classes> myData = mAttendanceFormClient.appData("Classes", Classes.class);
                    final int finalI = i;
                    myData.get(q, new KinveyListCallback<Classes>() {
                        @Override
                        public void onFailure(Throwable t) {
                            Log.d(LOGTAG, "Error fetching data: " + t.getMessage());
                            return;
                        }

                        @Override
                        public void onSuccess(Classes[] getClasses) {
                            allclass[finalI] = getClasses[0];
                            if(allclass.length == studentClasses.length){
                                LoadClasses(studentClasses);
                            }
                        }
                    });
                }
                //LoadClasses(studentClasses);
            }});}

    private void LoadClasses(StudentClasses[] getClasses ) {

        String[]coursename = new String[getClasses.length];
        String[] clecture= new String[getClasses.length];
        String [] day=new String[getClasses.length];
        Integer[] courseCode = new Integer[getClasses.length];

        ArrayList<String> combineStudent = new ArrayList<String>();
        for(int i=0; i<getClasses.length; i++) {
            coursename[i] = allclass[i].getCourse_name();
            courseCode[i] = allclass[i].getCourse_code();
            clecture[i] =allclass[i].getCourse_lecturer();
            day[i]=allclass[i].getCourse_days();
            combineStudent.add( courseCode[i]+"   "+coursename[i]+"/n"+clecture[i]+" "+day[i]);
        }

        ArrayAdapter<String> adp=new ArrayAdapter<String>(ClassSelection.this, android.R.layout.simple_list_item_1,coursename);
        list.setAdapter(adp);
        list.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        alartforstudent(position);
                    }
                });
    }

    private void alartforstudent(final int numb){
        String Description = allclass[numb].getCourse_description();
        String Title = allclass[numb].getCourse_title();
        AlertDialog.Builder builder = new AlertDialog.Builder(ClassSelection.this);
        builder.setTitle(Title)
                .setMessage(Description)
                .setNegativeButton("Cancel", null)
                .setPositiveButton("Select Class", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog,int which){
                        StudentDashboard.setCurrentClass(allclass[numb]);
                        Intent intent_prof = new Intent(ClassSelection.this, StudentDashboard.class);
                        intent_prof.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent_prof.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent_prof);
                    }
                }).show();

    }

    private void alartforprofessor(final int numb){
        String Description = allclass[numb].getCourse_description();
        String Title = allclass[numb].getCourse_title();
        AlertDialog.Builder builder = new AlertDialog.Builder(ClassSelection.this);
        builder.setTitle(Title)
                .setMessage(Description)
                .setNegativeButton("Cancel", null)
                .setPositiveButton("Select Class", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog,int which){
                        ProfDashboard.setCurrentClass(allclass[numb]);
                        Intent intent_prof = new Intent(ClassSelection.this, ProfDashboard.class);
                        intent_prof.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent_prof.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent_prof);
                    }
                }).show();

    }

   /* private void NavigatetoLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); //Make sure back space doesnot navigate back to main screen
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_drawer, menu);
        return true;
    }

    //logoff user
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int itemId = item.getItemId();
        if(itemId == R.id.action_logoff){
            logout();
        }
        if(itemId == R.id.action_profile)
        {
            Intent i1= new Intent(ClassSelection.this,ProfileScreen.class);
            startActivity(i1);
        }
        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        setProgressBarIndeterminateVisibility(true);
        mAttendanceFormClient.user().logout().execute();
        Log.d(LOGTAG, "User logged out");
        setProgressBarIndeterminateVisibility(false);
        NavigatetoLogin();
    }*/

}
