package com.cs442.androidproject.attendanceform.Model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

/**
 * Created by dmm on 11/9/2016.
 */

public class StudentGrade extends GenericJson {

    @Key
    private Integer course_code;
    @Key
    private String date;
    @Key
    private Integer grade;
    @Key
    private String usersEmail;

    public void setCourse_code(Integer course_code) {
        this.course_code = course_code;
    }

    public Integer getCourse_code() {
        return course_code;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setUsersEmail(String usersEmail) {
        this.usersEmail = usersEmail;
    }

    public String getUsersEmail() {
        return usersEmail;
    }
}
