package com.cs442.androidproject.attendanceform;

/**
 * Created by ljh on 16/10/13.
 */

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.cs442.androidproject.attendanceform.Model.StudentAttendance;

import java.util.ArrayList;
import java.util.List;

public class ContentAdapter extends ArrayAdapter<StudentAttendance> {

    Context context;

    ArrayList<StudentAttendance> item = new ArrayList<StudentAttendance>();

    public ContentAdapter(Context context, ArrayList<StudentAttendance> sda) {
        super(context, 0, sda);
        this.context = context;
        this.item = item;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            /*LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.vlist, null);*/
            v = LayoutInflater.from(getContext()).inflate(R.layout.vlist, parent, false);

        }
        StudentAttendance st = getItem(position);

        TextView code = (TextView) convertView.findViewById(R.id.text1);
        TextView Date = (TextView) convertView.findViewById(R.id.text2);
        TextView astatus = (TextView) convertView.findViewById(R.id.text3);
        TextView uEmail = (TextView) convertView.findViewById(R.id.text4);

        code.setText(st.getCourse_code());
        Date.setText(st.getDate());
        astatus.setText(st.getAttendance_status());
        uEmail.setText(st.getUsersEmail());

        return v;


    }
}