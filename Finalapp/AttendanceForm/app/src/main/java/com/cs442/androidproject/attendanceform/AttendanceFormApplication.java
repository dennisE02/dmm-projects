package com.cs442.androidproject.attendanceform;

import android.app.Application;
import android.util.Log;

import com.kinvey.android.Client;
import com.kinvey.android.callback.KinveyPingCallback;


/**
 * Created by dmm on 11/2/2016.
 */

public class AttendanceFormApplication extends Application{
    final static String LOGTAG = "ATF";
    private Client mAttendanceFormClient;

    private String pushAppKey = "kid_HkQL4Cwle";
    private String pushAppSecret = "58741790d24a4ebb9ea1a79eced9aedc";

    @Override
    public void onCreate(){

        mAttendanceFormClient = new Client.Builder(pushAppKey, pushAppSecret, this.getApplicationContext()).build();


        mAttendanceFormClient.ping(new KinveyPingCallback() {
            @Override
            public void onSuccess(Boolean aBoolean) {
                Log.d(LOGTAG, "Kinvey ping success");
            }

            @Override
            public void onFailure(Throwable throwable) {
                Log.d(LOGTAG, "Kinvey ping failure");
            }
        });
    }

    public void setmAttendanceFormClient(Client mAttendanceFormClient){
        this.mAttendanceFormClient = mAttendanceFormClient;
    }

    public Client getmAttendanceFormClient() {
        return this.mAttendanceFormClient;
    }
}
