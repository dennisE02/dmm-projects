package com.cs442.androidproject.attendanceform;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.cs442.androidproject.attendanceform.Model.StudentAbsent;
import com.kinvey.android.AsyncAppData;
import com.kinvey.android.Client;
import com.kinvey.java.core.KinveyClientCallback;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Student_absent extends AppCompatActivity {
    CheckBox absent;
    Button submit;
    EditText reason;

    private Client mAttendanceFormClient;
    final static String LOGTAG = "ATF";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.absent_student);

        absent = (CheckBox)findViewById(R.id.checkBox_absent);
        submit=(Button)findViewById(R.id.btnSubmit);
        reason=(EditText)findViewById(R.id.etReason);

        mAttendanceFormClient = ((AttendanceFormApplication)getApplication()).getmAttendanceFormClient();

        submit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //TODO deal with the reason
                if(absent.isChecked()){
                    String Reason = reason.getText().toString();
                    if(Reason.isEmpty()){
                        Reason  = "No reason given by student!";
                    }
                    SubmitAbsenceReport(Reason);
                    Toast.makeText(Student_absent.this, "Sumbmit success", Toast.LENGTH_LONG ).show();

                }
                else{
                    Toast.makeText(Student_absent.this, "make sure in the box", Toast.LENGTH_LONG ).show();
                }
                finish();
            }
        });
    }
    private void SubmitAbsenceReport(String reason) {
        String useremail = MainActivity.getCurrentUser().getUsersEmail();

        Date createdDate = new Date(java.lang.System.currentTimeMillis());
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy", Locale.US);
        String dateString = sdf.format(createdDate);

        Integer coursecode = StudentDashboard.getCurrentClass().getCourse_code();

        String RequestID = MainActivity.getCurrentUser().getUsername().toUpperCase() + dateString;

        StudentAbsent newRequest = new StudentAbsent();

        newRequest.setCourse_Code(coursecode);
        newRequest.setDate(dateString);
        newRequest.setRequest_id(RequestID);
        newRequest.setUsersEmail(useremail);
        newRequest.setStudent_reason(reason);

        AsyncAppData<StudentAbsent> mydata = mAttendanceFormClient.appData("StudentAbsent", StudentAbsent.class);
        mydata.save(newRequest, new KinveyClientCallback<StudentAbsent>() {
            @Override
            public void onFailure(Throwable e) {
                Log.e("TAG", "failed to save event data", e);
            }
            @Override
            public void onSuccess(StudentAbsent nRequest) {
                Log.d(LOGTAG, "Student Absent uploaded succesfully!");
            }
        });

    }

    private void NavigatetoLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); //Make sure back space doesnot navigate back to main screen
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_drawer, menu);
        return true;
    }

    //logoff user
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int itemId = item.getItemId();
        if(itemId == R.id.action_logoff){
            logout();
        }
        if(itemId == R.id.action_profile)
        {
            Intent i1= new Intent(Student_absent.this,ProfileScreen.class);
            startActivity(i1);
        }
        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        setProgressBarIndeterminateVisibility(true);
        mAttendanceFormClient.user().logout().execute();
        Log.d(LOGTAG, "User logged out");
        setProgressBarIndeterminateVisibility(false);
        NavigatetoLogin();
    }
}
