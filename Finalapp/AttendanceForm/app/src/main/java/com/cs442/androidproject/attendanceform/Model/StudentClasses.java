package com.cs442.androidproject.attendanceform.Model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

/**
 * Created by dmm on 11/11/2016.
 */

public class StudentClasses extends GenericJson {
    @Key
    private String usersEmail;
    @Key
    private Integer course_code;

    public void setUsersEmail(String usersEmail) {
        this.usersEmail = usersEmail;
    }

    public String getUsersEmail() {
        return usersEmail;
    }

    public void setCourse_code(Integer course_code) {
        this.course_code = course_code;
    }


    public Integer getCourse_code() {
        return course_code;
    }
    public StudentClasses(){

    }
}
