package com.cs442.androidproject.attendanceform;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.cs442.androidproject.attendanceform.Model.Users;
import com.kinvey.android.AsyncAppData;
import com.kinvey.android.Client;
import com.kinvey.android.callback.KinveyListCallback;
import com.kinvey.android.callback.KinveyUserCallback;
import com.kinvey.java.Query;
import com.kinvey.java.User;
import com.kinvey.java.core.KinveyClientCallback;

public class LoginActivity extends AppCompatActivity {

    protected TextView mSignUp;
    Button log;
    private EditText user,password;
    final static String LOGTAG = "ATF";
    private Client mAttendanceFormClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set up Progressbar
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_login);
        user = (EditText) findViewById(R.id.usernameET);
        password = (EditText) findViewById(R.id.passwordET);

        mSignUp = (TextView)findViewById(R.id.tvSignUp);
        mSignUp.setOnClickListener(new View.OnClickListener(){
           @Override
            public void onClick(View v){
               Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
               startActivity(intent);
           }
        });
        //TODO added by dennis
        mAttendanceFormClient = ((AttendanceFormApplication)getApplication()).getmAttendanceFormClient();


        log=(Button)findViewById(R.id.loginBtn);
        log.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String username = user.getText().toString().trim();
                String Password = password.getText().toString().trim();

                //user.setText("");
                //password.setText("");
                //TODO start of - added by dennis
                if(username.isEmpty() || Password.isEmpty() ){
                    AlertDialog.Builder builder= new AlertDialog.Builder(LoginActivity.this);
                    builder.setMessage(R.string.login_error_message)
                            .setTitle(R.string.login_error_Title)
                            .setPositiveButton(android.R.string.ok, null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }else{
                    setProgressBarIndeterminateVisibility(true);
                    mAttendanceFormClient.user().login(username, Password, new KinveyUserCallback() {
                        @Override
                        public void onSuccess(User user) {
                            setProgressBarIndeterminateVisibility(false);

                            //fetch details of the user from main activity
                            Log.d(LOGTAG, "fetching user details");
                            setCurrentUser(user.getUsername());

                        }
                        @Override
                        public void onFailure(Throwable throwable) {
                            Log.d(LOGTAG, "Kinvey Error - " + throwable.toString());
                            setProgressBarIndeterminateVisibility(false);
                            CharSequence text = "Username already exists.";
                            Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                            toast.show();
                        }
                    });
                }
            }
        });
    }

    private void setCurrentUser(String nusername){

        final Users cUser = new Users();
        Query q = mAttendanceFormClient.query();
        q.equals("username", nusername);

        final AsyncAppData<Users> myData = mAttendanceFormClient.appData("Users", Users.class);
        myData.get(q, new KinveyListCallback<Users>() {
            @Override
            public void onFailure(Throwable t) {
                Log.d(LOGTAG, "Error fetching data: " + t.getMessage());
                return;
            }
            @Override
            public void onSuccess(Users[] getUsers) {
                cUser.setName(getUsers[0].getName());
                cUser.setUsersEmail(getUsers[0].getUsersEmail());
                cUser.setStatus_Flag(getUsers[0].getStatus_Flag());
                cUser.setUsername(getUsers[0].getUsername());
                cUser.setPassword(getUsers[0].getPassword());

                Log.d(LOGTAG, "Retrieved current user details");
                //check if the user has not yet set a name
                String name ;
                try{
                    Log.d(LOGTAG, cUser.getName().toString());
                    name = cUser.getName();
                }catch (NullPointerException e){
                    name = "a";
                }
                if (name.equals("a")) {
                    AlertDialog.Builder builder= new AlertDialog.Builder(LoginActivity.this);
                    final EditText input = new EditText(LoginActivity.this);
                    input.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
                    builder.setMessage(R.string.user_add_name_label)
                            .setTitle(R.string.user_add_name_title)
                            .setView(input)
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //TODO Update database with new name
                                    cUser.setName(input.getText().toString());
                                     final AsyncAppData<Users> update = mAttendanceFormClient.appData("Users", Users.class);
                                        update.save(cUser, new KinveyClientCallback<Users>(){

                                            @Override
                                            public void onSuccess(Users users) {
                                                Log.d(LOGTAG, "update succesful " + users.getUsersEmail());
                                            }

                                            @Override
                                            public void onFailure(Throwable throwable) {
                                                Log.d(LOGTAG, throwable.getMessage());
                                            }
                                        });
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                    MainActivity.setCurrentUser(cUser);
                    NavigateToPage(cUser.getStatus_Flag());



            }
        });
    }

    private void NavigateToPage(String sFlag) {


        Log.d(LOGTAG, "received flag is " + sFlag);



        if (sFlag.equals("P")) {
            Intent intent_prof = new Intent(LoginActivity.this, ProfDashboard.class);
            intent_prof.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent_prof.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent_prof);
        }

        if (sFlag.equals("S")) {

            Toast.makeText(LoginActivity.this, "hello student", Toast.LENGTH_LONG).show();
            Intent intent_stu = new Intent(LoginActivity.this, StudentDashboard.class);
            intent_stu.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent_stu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent_stu);
        }



    }
}
