package com.cs442.androidproject.attendanceform.Model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

/**
 * Created by dmm on 11/9/2016.
 */

public class Users extends GenericJson {
    @Key
    private String username;
    @Key
    private String password;
    @Key
    private String Name;
    @Key
    private String usersEmail;
    @Key
    private String status_Flag;


    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getName() {
        return Name;
    }

    public void setUsersEmail(String usersEmail) {
        this.usersEmail = usersEmail;
    }

    public String getUsersEmail() {
        return usersEmail;
    }

    public void setStatus_Flag(String status_Flag) {
        this.status_Flag = status_Flag;
    }

    public String getStatus_Flag() {
        return status_Flag;
    }

    public Users(){}
}
