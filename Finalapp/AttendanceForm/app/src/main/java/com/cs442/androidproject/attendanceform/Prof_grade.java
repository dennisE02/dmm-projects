package com.cs442.androidproject.attendanceform;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.cs442.androidproject.attendanceform.Model.Classes;
import com.cs442.androidproject.attendanceform.Model.StudentClasses;
import com.cs442.androidproject.attendanceform.Model.StudentGrade;
import com.cs442.androidproject.attendanceform.Model.Users;
import com.kinvey.android.AsyncAppData;
import com.kinvey.android.Client;
import com.kinvey.android.callback.KinveyListCallback;
import com.kinvey.java.Query;
import com.kinvey.java.core.KinveyClientCallback;
import com.kinvey.java.query.AbstractQuery;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class Prof_grade extends AppCompatActivity {

    Users currentUser = MainActivity.getCurrentUser();
    Classes currentClass = ProfDashboard.getCurrentClass();
    final static String LOGTAG = "ATF";
    public final static String EXTRA_MESSAGE = "com.cs442.group18.attendanceform";

    private Client mAttendanceFormClient;
    Calendar mCalendar;
    Button submitGrade;
    EditText etDate;
    String stdMail;
    TextView gradesummary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.prof_grade_student);
        mCalendar = Calendar.getInstance();
        mAttendanceFormClient = ((AttendanceFormApplication)getApplication()).getmAttendanceFormClient();

        Intent intent = getIntent();
        String data[] = intent.getStringArrayExtra(EXTRA_MESSAGE);

        String stdName = data[0];
        stdMail = data[1];
        fetchAllGrade();

        TextView labelName = (TextView)findViewById(R.id.studentNameGrade);
        labelName.setText(stdName);

        gradesummary = (TextView)findViewById(R.id.gradesummary);

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mCalendar.set(Calendar.YEAR, year);
                mCalendar.set(Calendar.MONTH, month);
                mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };

        final EditText grade = (EditText)findViewById(R.id.NewGrade);

        etDate = (EditText)findViewById(R.id.etDate);
        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(Prof_grade.this, date, mCalendar
                        .get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),
                        mCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        submitGrade = (Button)findViewById(R.id.btnNewGrade);
        submitGrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(grade.getText().toString().equals(null)){
                    //TODO Zhou alert professor to enter grade
                }else
                    newGrade(Integer.parseInt(grade.getText().toString()));
            }
        });


    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    private void newGrade(int ngrade){
        StudentGrade studentGrade = new StudentGrade();
        studentGrade.setGrade(ngrade);
        studentGrade.setUsersEmail(stdMail);
        studentGrade.setDate(etDate.getText().toString());//something here
        studentGrade.setCourse_code(currentClass.getCourse_code());

        AsyncAppData<StudentGrade> newstudentgrade = mAttendanceFormClient.appData("Student_Grades", StudentGrade.class);

        newstudentgrade.save(studentGrade, new KinveyClientCallback<StudentGrade>() {
            @Override
            public void onSuccess(StudentGrade studentGrade) {
                Log.i(LOGTAG, "Grade succesfully entered!");
                AlertDialog.Builder builder= new AlertDialog.Builder(Prof_grade.this);
                builder.setMessage(R.string.grading_succes_label)
                        .setTitle(R.string.grading_succes_title)
                        .setPositiveButton(android.R.string.ok, null);
                //TODO work out here
                AlertDialog dialog = builder.create();
                dialog.show();
            }

            @Override
            public void onFailure(Throwable throwable) {
                Log.i(LOGTAG, throwable.getMessage());
            }
        });

    }

    private void fetchAllGrade(){

        Integer courseCode = ProfDashboard.getCurrentClass().getCourse_code(); //Acquire from Profdashboard


        Query q = mAttendanceFormClient.query();
        q.equals("course_code", courseCode);
        q.equals("usersEmail", stdMail);
        q.addSort("date", AbstractQuery.SortOrder.DESC);

        AsyncAppData<StudentGrade> myData = mAttendanceFormClient.appData("StudentGrade", StudentGrade.class);
        myData.get(q, new KinveyListCallback<StudentGrade>() {
            @Override
            public void onFailure(Throwable t) {
                Log.d(LOGTAG, "Error fetching data: " + t.getMessage());
                return;
            }
            @Override
            public void onSuccess(StudentGrade[] getGrades) {
                //TODO Zhou put the result in the listview
                Log.d(LOGTAG, "Grade : Success " + getGrades.length);

                Integer grades = 0;
                String[] date = new String[getGrades.length];
                Integer[] grade = new Integer[getGrades.length];

                ArrayList<String> combineStudent = new ArrayList<String>();
                for(int i=0; i<getGrades.length; i++) {
                    date[i] = getGrades[i].getDate();
                    grade[i] = getGrades[i].getGrade();
                    grades += grade[i];
                    combineStudent.add( "Date: " + date[i]+ "\r\nGrade: " + grade[i].toString());
                }

                gradesummary.setText("Total Grades : " + grades.toString());
                ArrayAdapter<String> adp=new ArrayAdapter<String>(Prof_grade.this, android.R.layout.simple_list_item_1,combineStudent);
                ListView list = (ListView)findViewById(R.id.row_grade_history);
                list.setAdapter(adp);

            }
        });
    }

    private void updateLabel() {

        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        etDate.setText(sdf.format(mCalendar.getTime()));
    }

    private void NavigatetoLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); //Make sure back space doesnot navigate back to main screen
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_drawer, menu);
        return true;
    }

    //logoff user
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int itemId = item.getItemId();
        if(itemId == R.id.action_logoff){
            logout();
        }
        if(itemId == R.id.action_profile)
        {
            Intent i1= new Intent(Prof_grade.this,ProfileScreen.class);
            startActivity(i1);
        }
        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        setProgressBarIndeterminateVisibility(true);
        mAttendanceFormClient.user().logout().execute();
        Log.d(LOGTAG, "User logged out");
        setProgressBarIndeterminateVisibility(false);
        NavigatetoLogin();
    }

}
