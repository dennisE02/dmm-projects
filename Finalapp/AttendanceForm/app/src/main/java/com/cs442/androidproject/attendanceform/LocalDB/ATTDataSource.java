package com.cs442.androidproject.attendanceform.LocalDB;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.cs442.androidproject.attendanceform.Model.NotificationItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dmm on 11/9/2016.
 */

public class ATTDataSource {

    public static final String LOGTAG = "ATF";

    SQLiteOpenHelper dbhelper;
    SQLiteDatabase database;

    private  static final String[] allNotifications = {
            ATTOpenHelper.COLUMN_ID,
            ATTOpenHelper.COLUMN_TITLE,
            ATTOpenHelper.COLUMN_MESS,
            ATTOpenHelper.COLUMN_FLAG,
            ATTOpenHelper.COLUMN_DATE
    };

    public ATTDataSource(Context context){
        dbhelper = new ATTOpenHelper(context);
    }

    public void open(){
        Log.i(LOGTAG, "Database Opened");
        database = dbhelper.getWritableDatabase();
    }

    public void close(){
        Log.i(LOGTAG, "Database Closed");
        dbhelper.close();
    }

    /*Notification Table*/
    public NotificationItem create(NotificationItem NT){
        ContentValues values = new ContentValues();
        values.put(ATTOpenHelper.COLUMN_TITLE, NT.getTitle());
        values.put(ATTOpenHelper.COLUMN_MESS, NT.getMess());
        values.put(ATTOpenHelper.COLUMN_FLAG, NT.getFlag());
        values.put(ATTOpenHelper.COLUMN_DATE, NT.getDatetime());

        long insertid = database.insert(ATTOpenHelper.TABLE_NOTIFICATION, null, values);
        NT.setId(insertid);

        return NT;
    }

    public List<NotificationItem> findAll(){
        List<NotificationItem> NTs = new ArrayList<NotificationItem>();

        Cursor cursor = database.query(ATTOpenHelper.TABLE_NOTIFICATION, allNotifications, null, null, null, null, ATTOpenHelper.COLUMN_DATE + "ASC");

        Log.i(LOGTAG, "Returned " + cursor.getCount() + " rows");
        if(cursor.moveToFirst()){
            do{
                NotificationItem NT = new NotificationItem();
                NT.setId(cursor.getLong(cursor.getColumnIndex(ATTOpenHelper.COLUMN_ID)));
                NT.setTitle(cursor.getString(cursor.getColumnIndex(ATTOpenHelper.COLUMN_TITLE)));
                NT.setMess(cursor.getString(cursor.getColumnIndex(ATTOpenHelper.COLUMN_MESS)));
                NT.setFlag(cursor.getString(cursor.getColumnIndex(ATTOpenHelper.COLUMN_FLAG)));
                NT.setDatetime(cursor.getString(cursor.getColumnIndex(ATTOpenHelper.COLUMN_DATE)));

                NTs.add(NT);
            }while(cursor.moveToNext());
        }
        cursor.close();
        return NTs;
    }

    public boolean removeNotification(NotificationItem NT){
        String where  = ATTOpenHelper.COLUMN_ID + "=" + NT.getId();
        int result = database.delete(ATTOpenHelper.TABLE_NOTIFICATION, where, null);
        return (result == 1);
    }


}
