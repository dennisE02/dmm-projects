package com.cs442.androidproject.attendanceform.Model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

/**
 * Created by dmm on 11/9/2016.
 */

public class Classes extends GenericJson {

    @Key
    private Integer course_code;
    @Key
    private String course_title;
    @Key
    private String course_name;
    @Key
    private String course_description;
    @Key
    private String course_lecturer;
    @Key
    private String course_days;

    public void setCourse_code(Integer course_code) {
        this.course_code = course_code;
    }

    public Integer getCourse_code() {
        return course_code;
    }

    public void setCourse_title(String course_title) {
        this.course_title = course_title;
    }

    public String getCourse_title() {
        return course_title;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_description(String course_description) {
        this.course_description = course_description;
    }

    public String getCourse_description() {
        return course_description;
    }

    public void setCourse_lecturer(String course_lecturer) {
        this.course_lecturer = course_lecturer;
    }

    public String getCourse_lecturer() {
        return course_lecturer;
    }

    public void setCourse_days(String course_days) {
        this.course_days = course_days;
    }

    public String getCourse_days() {
        return course_days;
    }

    public Classes(){}
}
