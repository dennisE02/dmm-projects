package com.cs442.androidproject.attendanceform;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.cs442.androidproject.attendanceform.Model.Users;
import com.kinvey.android.AsyncAppData;
import com.kinvey.android.Client;
import com.kinvey.android.callback.KinveyUserCallback;
import com.kinvey.java.User;
import com.kinvey.java.core.KinveyClientCallback;

public class SignupActivity extends AppCompatActivity {

    final static String LOGTAG = "ATF";

    protected EditText mName;
    protected EditText mUsername;
    protected EditText mPassword;
    protected EditText mPassword1;
    protected EditText mEmail;
    protected Button mSignupButton;
    Client mAttendanceFormClient;
    Users newUser = new Users();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_signup);

        //let student know only professors can signup
        AlertDialog.Builder builder= new AlertDialog.Builder(SignupActivity.this);
        builder.setMessage(R.string.sign_warning_message)
                .setTitle(R.string.sign_warning_Title)
                .setPositiveButton(android.R.string.ok, null);
        AlertDialog dialog = builder.create();
        dialog.show();

      mAttendanceFormClient = ((AttendanceFormApplication)getApplication()).getmAttendanceFormClient();

        mName = (EditText)findViewById(R.id.editTextName);
        mUsername = (EditText)findViewById(R.id.editTextUserName);
        mPassword = (EditText)findViewById(R.id.editTextPassword);
        mPassword1 = (EditText)findViewById(R.id.editTextConfirmPassword);
        mEmail = (EditText)findViewById(R.id.editTextEmail);



        mSignupButton = (Button)findViewById(R.id.buttonCreateAccount);
        mSignupButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                final String name = mName.getText().toString().trim();
                final String username = mUsername.getText().toString().trim();
                final String password = mPassword.getText().toString().trim();
                String password1 = mPassword1.getText().toString().trim();
                final String email = mEmail.getText().toString().trim();
                // verify if password match
                if(username.isEmpty() || password.isEmpty() || email.isEmpty()|| name.isEmpty()){
                    Log.d(LOGTAG, "Error in enetered data");
                    AlertDialog.Builder builder= new AlertDialog.Builder(SignupActivity.this);
                    builder.setMessage(R.string.sign_error_message)
                            .setTitle(R.string.sign_error_Title)
                            .setPositiveButton(android.R.string.ok, null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }else{
                    //TODO LATER create the new user
                    setProgressBarIndeterminateVisibility(true);
                    mAttendanceFormClient.user().create(username, password, new KinveyUserCallback(){
                        @Override
                        public void onSuccess(User user) {
                            newUser.setName(name);
                            newUser.setUsername(username);
                            newUser.setPassword(password);
                            newUser.setUsersEmail(email);
                            newUser.setStatus_Flag("P");
                            saveuser();
                        }

                        @Override
                        public void onFailure(Throwable throwable) {
                            Log.d(LOGTAG, "KInvey Error - " + throwable.getMessage().toString());
                            setProgressBarIndeterminateVisibility(false);
                            CharSequence text = "Username already exists.";
                            Toast toast = Toast.makeText(getApplicationContext(), throwable.toString(), Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                            toast.show();
                        }
                    });
                }
            }
        });
    }

    private void saveuser(){


        AsyncAppData<Users> mydata = mAttendanceFormClient.appData("Users", Users.class);
        mydata.save(newUser, new KinveyClientCallback<Users>() {
            @Override
            public void onFailure(Throwable e) {
                Log.e("TAG", "failed to save event data", e);
            }
            @Override
            public void onSuccess(Users nUser) {
                Log.d("TAG", "saved data for entity "+ nUser.getUsersEmail());
                MainActivity.setCurrentUser(newUser);

                Log.d(LOGTAG, "On signup succes - open profdashboard");
                setProgressBarIndeterminateVisibility(false);
                Intent intent = new Intent(SignupActivity.this, ProfDashboard.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }


}
