package com.cs442.androidproject.attendanceform.AppData;

/**
 * Created by dmm on 11/13/2016.
 */

public class AttendanceItem {
    String stdEmail;
    String stdName;
    String stdATT;

    public String getStdATT() {
        return stdATT;
    }

    public void setStdATT(String stdATT) {
        this.stdATT = stdATT;
    }

    public String getStdEmail() {
        return stdEmail;
    }

    public void setStdEmail(String stdEmail) {
        this.stdEmail = stdEmail;
    }

    public String getStdName() {
        return stdName;
    }

    public void setStdName(String stdName) {
        this.stdName = stdName;
    }

    public  AttendanceItem(){}
}
