package com.cs442.androidproject.attendanceform.Model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

/**
 * Created by dmm on 11/9/2016.
 */

public class StudentAbsent extends GenericJson{
    @Key
    private String student_reason;
    @Key
    private String date;
    @Key
    private String usersEmail;
    @Key
    private Integer course_code;
    @Key
    private String lecturer_response;
    @Key
    private String request_id;


    public String getStudent_reason() {
        return student_reason;
    }

    public void setStudent_reason(String student_reason) {
        this.student_reason = student_reason;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUsersEmail() {
        return usersEmail;
    }

    public void setUsersEmail(String usersEmail) {
        this.usersEmail = usersEmail;
    }

    public Integer getCourse_Code() {
        return course_code;
    }

    public void setCourse_Code(Integer course_code) {
        this.course_code = course_code;
    }

    public String getLecturer_response() {
        return lecturer_response;
    }

    public void setLecturer_response(String lecturer_response) {
        this.lecturer_response = lecturer_response;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public StudentAbsent(){}


}
