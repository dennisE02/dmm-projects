package com.cs442.androidproject.attendanceform;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Toast;

import com.cs442.androidproject.attendanceform.Model.Users;
import com.kinvey.android.AsyncAppData;
import com.kinvey.android.Client;
import com.kinvey.android.callback.KinveyListCallback;
import com.kinvey.android.callback.KinveyUserCallback;
import com.kinvey.java.Query;
import com.kinvey.java.User;

public class MainActivity extends Activity {

    final static String LOGTAG = "ATF";
    private Client mAttendanceFormClient;

    public static Users currentUser = new Users();
    private static Users cUser = new Users();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_main);

        mAttendanceFormClient = ((AttendanceFormApplication)getApplication()).getmAttendanceFormClient();
        //check if someone is aready logon on the system
        setProgressBarIndeterminateVisibility(true);
        mAttendanceFormClient.user().retrieve(new KinveyUserCallback() {
            @Override
            public void onSuccess(User user) {
                setProgressBarIndeterminateVisibility(false);
                Log.d(LOGTAG, user.getUsername());
                //this is where i get the username of the current user - you can use it if you have need for it
                //here determine if current user is a professor or student using the userstatus and navigate to the approriate screen.


                //navigating to prof dashbaord for test purpose
                fetchUserdetails(user.getUsername());

            }

            @Override
            public void onFailure(Throwable throwable) {
                setProgressBarIndeterminateVisibility(false);
                Log.d(LOGTAG, "Kinvey failure : " + throwable.getMessage());
                NavigatetoLogin();
            }
        });
    }

    private void NavigateToPage(String sFlag) {


        Log.d(LOGTAG, "received flag is " + sFlag);



        if (sFlag.equals("P")) {
            Intent intent_prof = new Intent(MainActivity.this, ProfDashboard.class);
            intent_prof.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent_prof.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent_prof);
        }

        if (sFlag.equals("S")) {

            Toast.makeText(MainActivity.this, "hello student", Toast.LENGTH_LONG).show();
            Intent intent_stu = new Intent(MainActivity.this, StudentDashboard.class);
            intent_stu.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent_stu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent_stu);
        }



    }

    private void NavigatetoLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); //Make sure back space doesnot navigate back to main screen
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_drawer, menu);
        return true;
    }

    //logoff user
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int itemId = item.getItemId();
        if(itemId == R.id.action_logoff){
            logout();
        }
        if(itemId == R.id.action_profile)
        {
            Intent i1= new Intent(MainActivity.this,ProfileScreen.class);
            startActivity(i1);
        }
        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        setProgressBarIndeterminateVisibility(true);
        mAttendanceFormClient.user().logout().execute();
        Log.d(LOGTAG, "User logged out");
        setProgressBarIndeterminateVisibility(false);
        NavigatetoLogin();
    }

    public static Users getCurrentUser(){
        return currentUser;
    }

    public void fetchUserdetails(String nusername){

//        final Users cUser = new Users();
        Query q = mAttendanceFormClient.query();
        q.equals("username", nusername);

        AsyncAppData<Users> myData = mAttendanceFormClient.appData("Users", Users.class);
        myData.get(q, new KinveyListCallback<Users>() {
            @Override
            public void onFailure(Throwable t) {
                Log.d(LOGTAG, "Error fetching data: " + t.getMessage());
                return;
            }
            @Override
            public void onSuccess(Users[] getUsers) {
                cUser.setName(getUsers[0].getName());
                cUser.setUsersEmail(getUsers[0].getUsersEmail());
                cUser.setStatus_Flag(getUsers[0].getStatus_Flag());
                cUser.setUsername(getUsers[0].getUsername());
                cUser.setPassword(getUsers[0].getPassword());

                Log.d(LOGTAG, "Retrieved current user details");

                currentUser = cUser;
                Log.d(LOGTAG, currentUser.getUsersEmail());
                NavigateToPage(cUser.getStatus_Flag());


            }
        });

    }

    public static void setCurrentUser(Users newUser){
        currentUser = newUser;
        Log.d(LOGTAG, "Current user" + currentUser.getName() + " set in main activity");
    }
}

/*
attedance,absent,grade,profile,notification,class screem for professor
 */