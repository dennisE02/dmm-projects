package com.cs442.androidproject.attendanceform;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs442.androidproject.attendanceform.AppData.AttendanceItem;
import com.cs442.androidproject.attendanceform.Model.StudentAbsent;
import com.cs442.androidproject.attendanceform.Model.StudentAttendance;
import com.cs442.androidproject.attendanceform.Model.StudentGrade;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.kinvey.android.AsyncAppData;
import com.kinvey.android.Client;
import com.kinvey.android.callback.KinveyListCallback;
import com.kinvey.java.Query;
import com.kinvey.java.core.KinveyClientCallback;
import com.kinvey.java.query.AbstractQuery;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class Student_attendance extends AppCompatActivity {
    CheckBox ontime, late;
    Button submit;
    Button btn1;
    EditText reason;
    TextView gps;
    private LocationManager lm;
    private LocationListener ll;

    private Client mAttendanceFormClient;
    List<AttendanceItem> newList = new ArrayList<AttendanceItem>();
    ArrayList<String> combineStudent;
    ArrayAdapter<String> adp;
    final static String LOGTAG = "ATF";
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 10:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    configureButton();
                return;
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.student_attendance);

        ontime = (CheckBox) findViewById(R.id.checkBox_attedence);
        late = (CheckBox) findViewById(R.id.checkBox_late);
        submit = (Button) findViewById(R.id.submit_student);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit_student();
            }
        });
        btn1 = (Button) findViewById(R.id.button);
        reason = (EditText) findViewById(R.id.etReason);
        gps = (TextView) findViewById(R.id.textViewgps);

        lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        ll = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Double lat = location.getLatitude();
                Double lon = location.getLongitude();
/*it prints latitude of location*/
                if (location != null) {

                    Log.e("GPS", "location changed: lat=" + lat + ", lon=" + lon);
                    gps.setText(String.valueOf(lat));
                    // gps.setText(String.valueOf(lon));
                } else {
                    gps.setText("Provider not available");
                    //gps.setText("Provider not available");
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            //USed when GPS is off
            @Override
            public void onProviderDisabled(String provider) {
                Intent i1 = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(i1);
            }
        };
        //Check for permission as SDK
        //TODO preyang i commented out this because i produced an error try fixing it
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{
                    Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET
            }, 10);
        } else {
            configureButton();
        }
        ;


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        mAttendanceFormClient = ((AttendanceFormApplication)getApplication()).getmAttendanceFormClient();
        combineStudent = new ArrayList<String>();

        adp=new ArrayAdapter<String>(Student_attendance.this, android.R.layout.simple_list_item_1, combineStudent);

        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adp);
    }


    public void submit_student() {
        if (ontime.isChecked() && late.isChecked())
            Toast.makeText(Student_attendance.this, "Choose only one please, teacher will confuse.", Toast.LENGTH_LONG).show();
        else if (late.isChecked()) {
            //TODO deal with the reason
            String Reason = reason.getText().toString();
            if(Reason.isEmpty()){
                Reason  = "No reason given by student!";
            }
            SubmitAbsenceReport(Reason);
            Toast.makeText(Student_attendance.this, "Sumbmit success", Toast.LENGTH_LONG).show();
            finish();
        } else if (ontime.isChecked()) {
            submitAttendance();
            Toast.makeText(Student_attendance.this, "Sumbmit success", Toast.LENGTH_LONG).show();
            finish();
        } else
            Toast.makeText(Student_attendance.this, "Make choice in the box", Toast.LENGTH_LONG).show();

    }

    private void submitNewGrade(String date, Integer coursecode, String mail) {

        StudentGrade newRequest = new StudentGrade();
        newRequest.setCourse_code(coursecode);
        newRequest.setDate(date);
        newRequest.setUsersEmail(mail);
        newRequest.setGrade(15);

        AsyncAppData<StudentGrade> mydata = mAttendanceFormClient.appData("StudentGrade", StudentGrade.class);
        mydata.save(newRequest, new KinveyClientCallback<StudentGrade>() {
            @Override
            public void onFailure(Throwable e) {
                Log.e("TAG", "failed to save event data", e);
            }
            @Override
            public void onSuccess(StudentGrade nRequest) {
                Log.d(LOGTAG, "Student Grade uploaded succesfully!");
            }
        });
    }

    private void submitAttendance() {
        String Status = "P";
        final String usermail  = MainActivity.getCurrentUser().getUsersEmail();
        final Integer coursecode = StudentDashboard.getCurrentClass().getCourse_code();

        Date createdDate = new Date(java.lang.System.currentTimeMillis());
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy", Locale.US);
        final String dateString = sdf.format(createdDate);

        StudentAttendance newRequest = new StudentAttendance();
        newRequest.setCourse_code(coursecode);
        newRequest.setDate(dateString);
        newRequest.setUsersEmail(usermail);
        newRequest.setAttendance_status(Status);

        AsyncAppData<StudentAttendance> mydata = mAttendanceFormClient.appData("StudentAttendance", StudentAttendance.class);
        mydata.save(newRequest, new KinveyClientCallback<StudentAttendance>() {
            @Override
            public void onFailure(Throwable e) {
                Log.e("TAG", "failed to save event data", e);
            }
            @Override
            public void onSuccess(StudentAttendance nRequest) {
                Log.d(LOGTAG, "Student Attendance uploaded succesfully!");
                submitNewGrade(dateString, coursecode, usermail);
            }
        });

    }

    private void SubmitAbsenceReport(String reason) {
        String useremail = MainActivity.getCurrentUser().getUsersEmail();

        Date createdDate = new Date(java.lang.System.currentTimeMillis());
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy", Locale.US);
        String dateString = sdf.format(createdDate);

        Integer coursecode = StudentDashboard.getCurrentClass().getCourse_code();

        String RequestID = MainActivity.getCurrentUser().getUsername().toUpperCase() + dateString;

        StudentAbsent newRequest = new StudentAbsent();

        newRequest.setCourse_Code(coursecode);
        newRequest.setDate(dateString);
        newRequest.setRequest_id(RequestID);
        newRequest.setUsersEmail(useremail);
        newRequest.setStudent_reason(reason);

        AsyncAppData<StudentAbsent> mydata = mAttendanceFormClient.appData("StudentAbsent", StudentAbsent.class);
        mydata.save(newRequest, new KinveyClientCallback<StudentAbsent>() {
            @Override
            public void onFailure(Throwable e) {
                Log.e("TAG", "failed to save event data", e);
            }
            @Override
            public void onSuccess(StudentAbsent nRequest) {
               Log.d(LOGTAG, "Student Absent uploaded succesfully!");
            }
        });

    }

    private void configureButton() {
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // first parameter provider, then refresh interval, then min distance that can be used for refresh location automatically
                // when  you move  this distance, location listner
                if (ActivityCompat.checkSelfPermission(Student_attendance.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Student_attendance.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                lm.requestLocationUpdates("gps", 5000, 10, ll);
            }
        });

    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Student_attendance Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    private void getAttendances(){

//        String cdate = etDate.getText().toString();
        String user = MainActivity.getCurrentUser().getUsersEmail();
        Integer classcode = StudentDashboard.getCurrentClass().getCourse_code();

        final StudentAttendance st = new StudentAttendance();
        Query q = mAttendanceFormClient.query();
        q.equals("usersEmail", user);
        q.equals("course_code" , classcode);
        q.addSort("date", AbstractQuery.SortOrder.DESC);

        final AsyncAppData<StudentAttendance> myData = mAttendanceFormClient.appData("StudentAttendance", StudentAttendance.class);
        myData.get(q, new KinveyListCallback<StudentAttendance>() {
            @Override
            public void onSuccess(final StudentAttendance[] studentAttendances) {
                //Summary of class attendance
                Log.d(LOGTAG, "Student_Attendance : getAttendance Success");

                for (StudentAttendance i : studentAttendances){
                    Log.d(LOGTAG, "student -> " + i.toString());

                    combineStudent.add("Date - " + i.getDate() + "\r\t\t" + "Status - " + i.getAttendance_status());
                    adp.notifyDataSetChanged();


                }
            }

            @Override
            public void onFailure(Throwable throwable) {
                Log.d(LOGTAG, "Prof_Attendance : Error fetching data: " + throwable.getMessage());
                return;
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        getAttendances();
    }

    private void NavigatetoLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); //Make sure back space doesnot navigate back to main screen
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_drawer, menu);
        return true;
    }

    //logoff user
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int itemId = item.getItemId();
        if(itemId == R.id.action_logoff){
            logout();
        }
        if(itemId == R.id.action_profile)
        {
            Intent i1= new Intent(Student_attendance.this,ProfileScreen.class);
            startActivity(i1);
        }
        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        setProgressBarIndeterminateVisibility(true);
        mAttendanceFormClient.user().logout().execute();
        Log.d(LOGTAG, "User logged out");
        setProgressBarIndeterminateVisibility(false);
        NavigatetoLogin();
    }

    /**
     * Created by shahp on 11/27/2016.
     */

    public static class ProfessorAddClass extends AppCompatActivity {
        final static String LOGTAG = "ATF";

    }
}
