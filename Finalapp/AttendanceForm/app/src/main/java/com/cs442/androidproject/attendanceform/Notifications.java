package com.cs442.androidproject.attendanceform;

import android.app.ListFragment;
import android.app.Notification;

import com.cs442.androidproject.attendanceform.Model.NotificationItem;

/**
 * Created by dmm on 11/12/2016.
 */

public class Notifications extends ListFragment {

    public static NotificationItem selectedNotification = new NotificationItem();


    public NotificationItem getSelectedNotification(){
        return selectedNotification;
    }

    public static void setSelectedNotification(NotificationItem selectedNotification){
        Notifications.selectedNotification = selectedNotification;
    }
}
