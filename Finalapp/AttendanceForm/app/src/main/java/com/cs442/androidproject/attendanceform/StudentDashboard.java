package com.cs442.androidproject.attendanceform;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.cs442.androidproject.attendanceform.Model.Classes;
import com.kinvey.android.Client;

/**
 * Created by ljh on 16/11/2.
 */

public class StudentDashboard extends AppCompatActivity{
    Button student_absent,student_attendance,student_grade;
    private Client mAttendanceFormClient;
    final static String LOGTAG = "ATF";

    public static Classes currentClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.student_dashboard);

        mAttendanceFormClient = ((AttendanceFormApplication)getApplication()).getmAttendanceFormClient();

        student_attendance=(Button)findViewById(R.id.attendence_professor);
        student_absent=(Button)findViewById(R.id.absent_professor);
        student_grade=(Button)findViewById(R.id.grade_professor);

        student_attendance.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(StudentDashboard.this, Student_attendance.class);
                startActivity(intent);
            }
        });
        student_absent.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(StudentDashboard.this, Student_absent.class);
                startActivity(intent);
            }
        });
        student_grade.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(StudentDashboard.this, Student_grade.class);
                startActivity(intent);
            }
        });

        //Launch Class Selection Screen
        String name;
        try{
            Log.d(LOGTAG, currentClass.getCourse_code().toString());
            name = String.valueOf(currentClass.getCourse_code());
        }catch (NullPointerException e){
            name = "no class";
        }
        if(name.equals("no class")){
            Intent intent = new Intent(StudentDashboard.this, ClassSelection.class);
            startActivity(intent);
        }

    }

    private void NavigatetoLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); //Make sure back space doesnot navigate back to main screen
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_drawer, menu);
        return true;
    }

    //logoff user
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int itemId = item.getItemId();
        if(itemId == R.id.action_logoff){
            logout();
        }
        if(itemId == R.id.action_profile)
        {
            Intent i1= new Intent(StudentDashboard.this,ProfileScreen.class);
            startActivity(i1);
        }
        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        setProgressBarIndeterminateVisibility(true);
        mAttendanceFormClient.user().logout().execute();
        Log.d(LOGTAG, "User logged out");
        setProgressBarIndeterminateVisibility(false);
        NavigatetoLogin();
    }

    public static Classes getCurrentClass(){
        return currentClass;
    }

    public static void setCurrentClass(Classes newClass){
        currentClass = newClass;
        Log.d(LOGTAG, "Current class " + currentClass.getCourse_code() + " set in professor activity");
    }


}
