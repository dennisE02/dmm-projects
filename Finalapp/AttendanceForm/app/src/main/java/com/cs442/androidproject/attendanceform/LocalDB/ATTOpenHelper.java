package com.cs442.androidproject.attendanceform.LocalDB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by dmm on 11/9/2016.
 */

public class ATTOpenHelper extends SQLiteOpenHelper {

    private static final String LOGTAG = "ATF";

    private static final String DATABASE_NAME = "attendanceform.db";
    private static final int DATABASE_VERSION = 1;

    /*Notificatio Table*/
    public static final String TABLE_NOTIFICATION = "attNotifications";
    public static final String COLUMN_TITLE = "notificationTitle";
    public static final String COLUMN_ID = "notificationId";
    public static final String COLUMN_MESS = "notificationMessage";
    public static final String COLUMN_FLAG = "notificationFlag";
    public static final String COLUMN_DATE = "notificationdDate";

    private static final String CREATE_NOTIFICATION =
            "CREATE TABLE " + TABLE_NOTIFICATION + " (" +
                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_TITLE + " TEXT, " +
                    COLUMN_MESS + " TEXT, " +
                    COLUMN_FLAG + " TEXT, " +
                    COLUMN_DATE + " TEXT " +
                    ")";

    public ATTOpenHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_NOTIFICATION);
        Log.i(LOGTAG, "Table has been created!");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_NOTIFICATION);
        onCreate(db);
    }
}
