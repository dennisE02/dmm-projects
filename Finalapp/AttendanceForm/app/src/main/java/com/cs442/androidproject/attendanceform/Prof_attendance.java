package com.cs442.androidproject.attendanceform;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.cs442.androidproject.attendanceform.AppData.AttendanceItem;
import com.cs442.androidproject.attendanceform.Model.Classes;
import com.cs442.androidproject.attendanceform.Model.StudentAttendance;
import com.cs442.androidproject.attendanceform.Model.Users;
import com.kinvey.android.AsyncAppData;
import com.kinvey.android.Client;
import com.kinvey.android.callback.KinveyListCallback;
import com.kinvey.java.Query;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by ljh on 16/11/3.
 */

public class Prof_attendance extends AppCompatActivity {

    Button searchRequest;
    EditText etDate;
    private Client mAttendanceFormClient;
    final static String LOGTAG = "ATF";
    Calendar mCalendar;
    List<AttendanceItem> newList = new ArrayList<AttendanceItem>();
    ArrayList<String> combineStudent;
    ArrayAdapter<String> adp;
    public final static String EXTRA_MESSAGE = "com.cs442.group18.attendanceform";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.prof_attendance);

        mCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mCalendar.set(Calendar.YEAR, year);
                mCalendar.set(Calendar.MONTH, month);
                mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };

        etDate = (EditText)findViewById(R.id.etDate);
        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(Prof_attendance.this, date, mCalendar
                        .get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),
                        mCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        searchRequest = (Button)findViewById(R.id.btnSearchGrade);
        searchRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAttendance();
            }
        });


        mAttendanceFormClient = ((AttendanceFormApplication)getApplication()).getmAttendanceFormClient();
        combineStudent = new ArrayList<String>();

        adp=new ArrayAdapter<String>(Prof_attendance.this, android.R.layout.simple_list_item_1, combineStudent);
        ListView listView = (ListView) findViewById(R.id.row_attendance);
        listView.setAdapter(adp);
        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        AttendanceItem item = newList.get(position);
                        String[] data = new String[4];
                                data[0] = item.getStdName();
                                data[1] = item.getStdEmail();
                        Intent ngrade = new Intent(Prof_attendance.this, Prof_grade.class);
                        ngrade.putExtra(EXTRA_MESSAGE, data);
                        startActivity(ngrade);
                    }
                });

    }

    private void updateLabel() {

        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        etDate.setText(sdf.format(mCalendar.getTime()));
    }

    private void getAttendance(){

        String cdate = etDate.getText().toString();
        Integer classcode = ProfDashboard.getCurrentClass().getCourse_code();

        if (cdate.equals(null)){
            return;
        }

        final StudentAttendance st = new StudentAttendance();
        Query q = mAttendanceFormClient.query();
        q.equals("date", cdate);
        q.equals("course_code" , classcode);

        final AsyncAppData<StudentAttendance> myData = mAttendanceFormClient.appData("StudentAttendance", StudentAttendance.class);
        myData.get(q, new KinveyListCallback<StudentAttendance>() {
            @Override
            public void onSuccess(final StudentAttendance[] studentAttendances) {
                //Summary of class attendance
                Log.d(LOGTAG, "Prof_Attendance : getAttendance Success");
                int totalAttendance =0;
                int totalAbsent = 0;
                int totalLate = 0;

                int counter = 0;
                for (StudentAttendance i : studentAttendances){
                    Log.d(LOGTAG, "student -> " + i.toString());
                    String a = i.getAttendance_status().toUpperCase();
                    if(a.equals("P")) {
                        totalAttendance += 1;
                    }else if(a.equals("A")) {
                        totalAbsent += 1;
                    }else if(a.equals("L")) {
                        totalLate += 1;
                    }
                    //fetch student Name
                    fetchUserName(counter, i.getUsersEmail(), i.getAttendance_status());
                    counter = counter + 1;
                }
                TextView summary = (TextView)findViewById(R.id.labeldesc);
                summary.setText("Class Total Attendance = " + totalAttendance +
                        "\r\nClass Total Absentees = " + totalAbsent +
                        "\r\nClass Total Late Comers = " + totalLate);
                //TODO Zhou - put the data studentAttendance into your content adapter
                //TODO - and display it in your list view
            }

            @Override
            public void onFailure(Throwable throwable) {
                Log.d(LOGTAG, "Prof_Attendance : Error fetching data: " + throwable.getMessage());
                return;
            }
        });

    }

    private void fetchUserName(final int callId, final String email, final String status){
        final AttendanceItem userATT = new AttendanceItem();
        Query q = mAttendanceFormClient.query();
        q.equals("usersEmail", email);
        final AsyncAppData<Users> myData = mAttendanceFormClient.appData("Users", Users.class);
        myData.get(q, new KinveyListCallback<Users>() {
            @Override
            public void onFailure(Throwable t) {
                Log.d(LOGTAG, "Error fetching data: " + t.getMessage());
                return;
            }

            @Override
            public void onSuccess(Users[] getUser) {

                userATT.setStdName(getUser[0].getName().toUpperCase());
                userATT.setStdEmail(email);
                userATT.setStdATT(status);

                newList.add(userATT);

                combineStudent.add(userATT.getStdName() + "\r\nStatus: " + userATT.getStdATT());
                adp.notifyDataSetChanged();


            }

        });
    }


    private void NavigatetoLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); //Make sure back space doesnot navigate back to main screen
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_drawer, menu);
        return true;
    }

    //logoff user
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int itemId = item.getItemId();
        if(itemId == R.id.action_logoff){
            logout();
        }
        if(itemId == R.id.action_profile)
        {
            Intent i1= new Intent(Prof_attendance.this,ProfileScreen.class);
            startActivity(i1);
        }
        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        setProgressBarIndeterminateVisibility(true);
        mAttendanceFormClient.user().logout().execute();
        Log.d(LOGTAG, "User logged out");
        setProgressBarIndeterminateVisibility(false);
        NavigatetoLogin();
    }
}
