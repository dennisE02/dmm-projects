package com.cs442.androidproject.attendanceform.Model;

import android.content.Context;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by dmm on 11/9/2016.
 */

public class NotificationItem {

    String mess;
    String title;
    String flag;
    String datetime;

    private long id;

    public long getId(){
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag){
        this.flag = flag;
    }

    public String getMess() {
        return mess;
    }

    public void setMess(String mess){
        this.mess = mess;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setDatetime(String datetime){
        this.datetime = datetime;
    }

    public String getDatetime(){
        return datetime;
    }

    public NotificationItem(String title, String mess, String flag){
        super();
        this.title = title;
        this.mess = mess;
        this.flag = flag;
    }

    public NotificationItem(){}


    public static String formatDateTime(Context context, String timeToFormat) {

        String finalDateTime = "";

        SimpleDateFormat iso8601Format = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");

        Date date = null;
        if (timeToFormat != null) {
            try {
                date = iso8601Format.parse(timeToFormat);
            } catch (ParseException e) {
                date = null;
            }

            if (date != null) {
                long when = date.getTime();
                int flags = 0;
                flags |= android.text.format.DateUtils.FORMAT_SHOW_TIME;
                flags |= android.text.format.DateUtils.FORMAT_SHOW_DATE;
                flags |= android.text.format.DateUtils.FORMAT_ABBREV_MONTH;
                flags |= android.text.format.DateUtils.FORMAT_SHOW_YEAR;

                finalDateTime = android.text.format.DateUtils.formatDateTime(context,
                        when + TimeZone.getDefault().getOffset(when), flags);
            }
        }
        return finalDateTime;
    }
}
