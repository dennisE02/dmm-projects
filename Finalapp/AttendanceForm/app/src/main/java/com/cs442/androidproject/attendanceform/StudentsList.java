package com.cs442.androidproject.attendanceform;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.cs442.androidproject.attendanceform.AppData.AttendanceItem;
import com.cs442.androidproject.attendanceform.Model.Classes;
import com.cs442.androidproject.attendanceform.Model.StudentClasses;
import com.cs442.androidproject.attendanceform.Model.Users;
import com.kinvey.android.AsyncAppData;
import com.kinvey.android.Client;
import com.kinvey.android.callback.KinveyListCallback;
import com.kinvey.java.Query;
import com.kinvey.java.User;

import java.util.ArrayList;
import java.util.List;

public class StudentsList extends AppCompatActivity {

    final static String LOGTAG = "ATF";
    private Client mAttendanceFormClient;
    final Users sUser = new Users();
    TextView className;

    public final static String EXTRA_MESSAGE = "com.cs442.group18.attendanceform";
    ArrayList<Users> newList;
    ArrayList<String> combineStudent;
    ArrayAdapter<String> adp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students_list);

        mAttendanceFormClient = ((AttendanceFormApplication)getApplication()).getmAttendanceFormClient();

        className = (TextView)findViewById(R.id.className);

        fetchAllStudent();

        combineStudent = new ArrayList<String>();
        newList = new ArrayList<Users>();

        adp=new ArrayAdapter<String>(StudentsList.this, android.R.layout.simple_list_item_1, combineStudent);
        ListView listView = (ListView) findViewById(R.id.stdList);
        listView.setAdapter(adp);
        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        //AttendanceItem item = newList.get(position);
                        Users item = newList.get(position);
                        String[] data = new String[4];
                        data[0] = item.getName();
                        data[1] = item.getUsersEmail();
                        Intent ngrade = new Intent(StudentsList.this, Prof_grade.class);
                        ngrade.putExtra(EXTRA_MESSAGE, data);
                        startActivity(ngrade);
                    }
                });
    }

    private void fetchAllStudent(){

        Classes cClass = ProfDashboard.getCurrentClass();
        className.setText(cClass.getCourse_name());
        StudentClasses allClass = new StudentClasses();

        Query q = mAttendanceFormClient.query();
        q.equals("course_code", cClass.getCourse_code());

        AsyncAppData<StudentClasses> myData = mAttendanceFormClient.appData("StudentClasses", StudentClasses.class);
        myData.get(q, new KinveyListCallback<StudentClasses>() {
            @Override
            public void onFailure(Throwable t) {
                Log.d(LOGTAG, "Error fetching data: " + t.getMessage());
                return;
            }
            @Override
            public void onSuccess(StudentClasses[] getClasses) {
                //lookup the student details using the email address
                for(int i = 0 ; i < getClasses.length; i++){
                    findStudentDetails(getClasses[i].getUsersEmail().toString());
                }
                Log.d(LOGTAG, "Classes fetched is " + getClasses.length);
            }
        });
    }

    private void findStudentDetails(final String email){

        Query q = mAttendanceFormClient.query();
        q.equals("usersEmail", email);

        AsyncAppData<Users> myData = mAttendanceFormClient.appData("Users", Users.class);
        myData.get(q, new KinveyListCallback<Users>() {
            @Override
            public void onFailure(Throwable t) {
                Log.d(LOGTAG, "Error fetching data: " + t.getMessage());
                return;
            }
            @Override
            public void onSuccess(Users[] getUsers) {

                newList.add(getUsers[0]);
                combineStudent.add(getUsers[0].getName().toUpperCase());
                adp.notifyDataSetChanged();

                Log.d(LOGTAG, "Retrieved studnet details");
            }
        });
    }

    private void NavigatetoLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); //Make sure back space doesnot navigate back to main screen
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_drawer, menu);
        return true;
    }

    //logoff user
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int itemId = item.getItemId();
        if(itemId == R.id.action_logoff){
            logout();
        }
        if(itemId == R.id.action_profile)
        {
            Intent i1= new Intent(StudentsList.this,ProfileScreen.class);
            startActivity(i1);
        }
        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        setProgressBarIndeterminateVisibility(true);
        mAttendanceFormClient.user().logout().execute();
        Log.d(LOGTAG, "User logged out");
        setProgressBarIndeterminateVisibility(false);
        NavigatetoLogin();
    }

}
