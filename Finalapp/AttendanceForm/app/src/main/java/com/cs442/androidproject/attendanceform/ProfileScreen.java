package com.cs442.androidproject.attendanceform;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cs442.androidproject.attendanceform.Model.Users;
import com.kinvey.android.Client;

import org.w3c.dom.Text;

/**
 * Created by preyang on 11/10/2016.
 */

public class ProfileScreen extends AppCompatActivity {

    private Client mAttendanceFormClient;
    final static String LOGTAG = "ATF";
    Users cUser =  new Users();

    protected TextView profileName;
    protected TextView profileBio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toast.makeText(ProfileScreen.this, " You are entered in the profile screen", Toast.LENGTH_LONG).show();
        setContentView(R.layout.profile_screen);

        mAttendanceFormClient = ((AttendanceFormApplication)getApplication()).getmAttendanceFormClient();
        profileName = (TextView)findViewById(R.id.user_profile_name);
        profileBio = (TextView)findViewById(R.id.user_profile_short_bio);
    }

    @Override
    protected void onResume() {
        super.onResume();

        cUser = MainActivity.getCurrentUser();

        if(cUser.getStatus_Flag().equals("P")){
            //do something
            profileName.setText(cUser.getName());
        }
        if(cUser.getStatus_Flag().equals("S")){
            //do something
            profileName.setText(cUser.getName());
        }
    }
}