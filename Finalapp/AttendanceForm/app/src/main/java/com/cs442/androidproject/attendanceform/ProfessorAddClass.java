package com.cs442.androidproject.attendanceform;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.cs442.androidproject.attendanceform.Model.Classes;
import com.google.api.client.util.Key;

/**
 * Created by ljh on 16/11/27.
 */

public class ProfessorAddClass extends AppCompatActivity {
    private EditText course_code, course_title, course_name, course_description, course_lecturer, course_days;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.professor_add_class_screen);
        course_code = (EditText) findViewById(R.id.editcode);
        course_title= (EditText) findViewById(R.id.editTitle);
        course_name= (EditText) findViewById(R.id.editname);
        course_description= (EditText) findViewById(R.id.editdesc);
       // course_lecturer= (EditText) findViewById(R.id.editText5);
       // course_days= (EditText) findViewById(R.id.editText6);
        Button submit = (Button)findViewById(R.id.buttonaddclass);
        submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                int code = Integer.parseInt(course_code.getText().toString());
                String title = course_title.getText().toString();
                String name = course_name.getText().toString();
                String description = course_description.getText().toString();
                //String lecturer = course_lecturer.getText().toString();
                //String day = course_days.getText().toString();
                if(code==0 || title.isEmpty()|| name.isEmpty()|| description.isEmpty() )
                { AlertDialog.Builder builder= new AlertDialog.Builder(ProfessorAddClass.this);
                    builder.setMessage(R.string.login_error_message)
                            .setTitle(R.string.login_error_Title)
                            .setPositiveButton(android.R.string.ok, null);
                    AlertDialog dialog = builder.create();
                    dialog.show();}
                else{
                    Classes newclass = new Classes();
                    newclass.setCourse_code(code);
                    //newclass.setCourse_days(day);
                    newclass.setCourse_description(description);
                    //newclass.setCourse_lecturer(lecturer);
                    newclass.setCourse_name(name);
                    newclass.setCourse_title(title);
                    //TODO add class detail to Classes

                }
            }
        });

    }
}

