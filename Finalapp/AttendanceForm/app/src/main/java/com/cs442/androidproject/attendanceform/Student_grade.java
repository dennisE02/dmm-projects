package com.cs442.androidproject.attendanceform;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.cs442.androidproject.attendanceform.Model.StudentGrade;
import com.kinvey.android.AsyncAppData;
import com.kinvey.android.Client;
import com.kinvey.android.callback.KinveyListCallback;
import com.kinvey.java.Query;
import com.kinvey.java.query.AbstractQuery;

import java.util.ArrayList;

/**
 * Created by ljh on 16/11/3.
 */

public class Student_grade extends AppCompatActivity{
    Button ok;
    final static String LOGTAG = "ATF";
    private Client mAttendanceFormClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.student_grade);
        ok = (Button)findViewById(R.id.btnOk);
        ok.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                finish();
            }
        });
        mAttendanceFormClient = ((AttendanceFormApplication)getApplication()).getmAttendanceFormClient();
    }

    @Override
    protected void onResume() {
        super.onResume();
        fetchAllGrade();
    }

    private void fetchAllGrade() {

        Integer courseCode = StudentDashboard.getCurrentClass().getCourse_code(); //Acquire from Profdashboard
        String stdMail = MainActivity.getCurrentUser().getUsersEmail();


        Query q = mAttendanceFormClient.query();
        q.equals("course_code", courseCode);
        q.equals("usersEmail", stdMail);
        q.addSort("date", AbstractQuery.SortOrder.DESC);

        AsyncAppData<StudentGrade> myData = mAttendanceFormClient.appData("StudentGrade", StudentGrade.class);
        myData.get(q, new KinveyListCallback<StudentGrade>() {
            @Override
            public void onFailure(Throwable t) {
                Log.d(LOGTAG, "Error fetching data: " + t.getMessage());
                return;
            }
            @Override
            public void onSuccess(StudentGrade[] getGrades) {
                //TODO Zhou put the result in the listview
                Log.d(LOGTAG, "Grade : Success " + getGrades.length);

                Integer grades = 0;
                String[] date = new String[getGrades.length];
                Integer[] grade = new Integer[getGrades.length];

                ArrayList<String> combineStudent = new ArrayList<String>();
                for(int i=0; i<getGrades.length; i++) {
                    date[i] = getGrades[i].getDate();
                    grade[i] = getGrades[i].getGrade();
                    combineStudent.add( "Date: " + date[i]+ "\r\nGrade: " + grade[i].toString());
                }


                ArrayAdapter<String> adp=new ArrayAdapter<String>(Student_grade.this, android.R.layout.simple_list_item_1,combineStudent);
                ListView list = (ListView)findViewById(R.id.listGrade);
                list.setAdapter(adp);

            }
        });
    }

    private void NavigatetoLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); //Make sure back space doesnot navigate back to main screen
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_drawer, menu);
        return true;
    }

    //logoff user
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int itemId = item.getItemId();
        if(itemId == R.id.action_logoff){
            logout();
        }
        if(itemId == R.id.action_profile)
        {
            Intent i1= new Intent(Student_grade.this,ProfileScreen.class);
            startActivity(i1);
        }
        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        setProgressBarIndeterminateVisibility(true);
        mAttendanceFormClient.user().logout().execute();
        Log.d(LOGTAG, "User logged out");
        setProgressBarIndeterminateVisibility(false);
        NavigatetoLogin();
    }

}
