/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordertrans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dmm
 */
public class TransactionDao {
    
    Connection conn = null;
    ResultSet rs = null;
    PreparedStatement pst = null;
    
    public TransactionDao(Connection conn){
        this.conn = conn;
    }
    
    /**
     * @define create Orders table
     * @throws SQLException
     */
    public void createOrderTable(){
        String sql ="CREATE TABLE IF NOT EXISTS orders (" +
                    "RefID varchar(10) NOT NULL," +
                    "ItemNumber varchar (255) NOT NULL, " +
                    "ItemDescription varchar(255) NOT NULL ," +
                    "AddressID varchar(16) NOT NULL," +
                    "CustomerCode varchar(6) NOT NULL," +
                    "FOREIGN KEY (AddressID) REFERENCES address(AddressID)," +
                    "FOREIGN KEY  (CustomerCode)  REFERENCES customer(Code)," +
                    "PRIMARY KEY (RefId, ItemNumber)" +
                    ")";
        
        
        try {
            pst = conn.prepareStatement(sql);
            
            pst.execute();
        
            System.out.println("Orders Table Created");
        
        } catch (SQLException ex) {
            Logger.getLogger(TransactionDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        //String indexSql = "CREATE UNIQUE INDEX itemRef ON orders (RefID, ItemNumber)";
    }
    
    /**
     * @define create Customer table
     * @throws SQLException
     */
    public void createCustomerTable(){
        String sql ="CREATE TABLE IF NOT EXISTS customer (" +
                    "Code varchar(6) PRIMARY KEY ," +
                    "FirstName varchar (255), " +
                    "LastName varchar(255) ," +
                    "Phone varchar(255)," +
                    "Email varchar(255)" +
                    ")";
        
        try {
            pst = conn.prepareStatement(sql);
            
            pst.execute();
        
            System.out.println("Customers Table Created");
        } catch (SQLException ex) {
            Logger.getLogger(TransactionDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    
    /**
     * @define create Address table
     * @throws SQLException
     */
    public void createAddressTable(){
        String sql = "CREATE TABLE IF NOT EXISTS address(" +
                    "AddressID varchar(16) NOT NULL," +
                    "AddressName varchar (255)," +
                    "AddressType varchar(255) ," +
                    "AddressLine1 varchar(255)," +
                    "AddressLine2 varchar(255)," +
                    "CountryCode varchar(6)," +
                    "PRIMARY KEY (AddressID)" +
                    ")";
        
        try {
            pst = conn.prepareStatement(sql);
            
            pst.execute();
        
            System.out.println("Address Tabe Created");
        
        } catch (SQLException ex) {
            Logger.getLogger(TransactionDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    /**
     * @param RefID Order Reference
     * @param ItemNo
     * @param ItemDesc
     * @param CusCode
     * @param AddrID
     * @returns 1 on success and 0 on failure
     */
    public int insertOrder(String RefID, String ItemNo, String ItemDesc, String CusCode, String AddrID){
        
        try{
            String sql = "INSERT INTO orders (RefID, ItemNumber, ItemDescription, AddressID, CustomerCode) "
                    + "values (?,?,?,?,?)";
            
            pst = conn.prepareStatement(sql);
            pst.setString(1, RefID);
            pst.setString(2, ItemNo);
            pst.setString(3, ItemDesc);
            pst.setString(4, AddrID);
            pst.setString(5, CusCode);
            
            pst.execute();
            
            return 1;
        }catch(Exception e){
            return 0;
        }
    }
    
    /**
     * 
     * @param FirstName
     * @param LastName
     * @param Phone
     * @param CusCode
     * @param Email
     * @return 1 on Success
     */
    public int insertCustomer(String FirstName, String LastName, String Phone, String CusCode, String Email){
        
        try{
            String sql = "INSERT INTO customer (Code, FirstName, LastName, Phone, Email) "
                    + "values (?,?,?,?,?)";
            
            pst = conn.prepareStatement(sql);
            pst.setString(1, CusCode);
            pst.setString(2, FirstName);
            pst.setString(3, LastName);
            pst.setString(4, Phone);
            pst.setString(5, Email);
            
            pst.execute();
            
            return 1;
        }catch(Exception e){
            return 0;
        }
    }
    
    /**
     * 
     * @param AddrID
     * @param AddrName
     * @param AddrType
     * @param AddrLine1
     * @param AddrLine2
     * @return 1 on success and 0 on failure
     */ 
    public int insertAddress(String AddrID, String AddrName, String AddrType, String AddrLine1, String AddrLine2, String CountryCode){
        
        try{
            String sql = "INSERT INTO address (AddressID, AddressName, AddressType, AddressLine1, AddressLine2, CountryCode) "
                    + "values (?,?,?,?,?,?)";
            
            pst = conn.prepareStatement(sql);
            pst.setString(1, AddrID);
            pst.setString(2, AddrName);
            pst.setString(3, AddrType);
            pst.setString(4, AddrLine1);
            pst.setString(5, AddrLine2);
            pst.setString(6, CountryCode);
            
            pst.execute();
            
            return 1;
        }catch(Exception e){
            return 0;
        }
    }

    public ResultSet seletOrders(){
        try{
            String sql = "SELECT RefID, ItemNumber, ItemDescription, AddressID, CustomerCode from orders";
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            
            return rs;
        }catch(Exception e){
           return null; 
        }
    }
    
   public ResultSet selectCustomer(String Code){
       try{
            String sql = "SELECT * from customer where Code = '"+Code+"'";
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            
            return rs;
        }catch(Exception e){
           return null; 
        }
   }
   
   public ResultSet selectAddress(String AddrID){
       try{
            String sql = "SELECT * from address where AddressID = '"+AddrID+"'";
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            
            return rs;
        }catch(Exception e){
           return null; 
        }
   }
   /**
    * 
    * @param AddrName
    * @param AddrType
    * @param AddrLine1
    * @param AddrLine2
    * @param CountryCode
    * @return AddressID if address exist in database
    */
   public ResultSet verifyAddress(String AddrName, String AddrType, String AddrLine1, String AddrLine2, String CountryCode){
        
        try{
            String sql = "SELECT AddressID FROM address WHERE AddressName = '"+AddrName+"' AND AddressType = '"+AddrType+"' AND"
            + " AddressLine1 = '"+AddrLine1+"' AND AddressLine2 = '"+AddrLine2+"' AND CountryCode = '"+CountryCode+"' ";
                    
            
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            
            return rs;
        }catch(Exception e){
            return null;
        }
    }
   
   public int updateCustomer(String FirstName, String LastName, String Phone, String CusCode, String Email){
                        
        try {
            String sql = "UPDATE customer set FirstName='"+ FirstName +"' , LastName='"+ LastName +"' , "
               + "Phone='"+ Phone +"' , Email='"+ Email +"' where Code='"+ CusCode + "' ";
            
            pst = conn.prepareStatement(sql);
            
            pst.execute();
            
            return 1;
        } catch (SQLException ex) {
            Logger.getLogger(TransactionDao.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
            
            
   }
   
   public int deleteOrder(String Ref, String ItemNo){
       try{
           String sql = "DELETE FROM orders where RefID = ? AND ItemNumber = ?";
           
           pst = conn.prepareStatement(sql);
           pst.setString(1, Ref);
           pst.setString(2, ItemNo);
            
            pst.execute();
            
            return 1;
       }catch (SQLException ex) {
            Logger.getLogger(TransactionDao.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
   }

   public int updateOrder(String ref, String oldItemNo,String newitemNo, String itemDesc){
       try {
            String sql = "UPDATE orders set ItemNumber='"+ newitemNo +"' , ItemDescription='"+ itemDesc +"'"
               + " where RefID='"+ ref + "' AND ItemNumber = '"+oldItemNo+"' ";
            
            pst = conn.prepareStatement(sql);
            
            pst.execute();
            
            return 1;
        } catch (SQLException ex) {
            Logger.getLogger(TransactionDao.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
   }
   
   public void close(){
        try {
            conn.close();
            pst.close();
            //rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(TransactionDao.class.getName()).log(Level.SEVERE, null, ex);
        }
       
   }
}
