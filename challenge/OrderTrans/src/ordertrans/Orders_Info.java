/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordertrans;

import java.awt.event.KeyEvent;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author dmm
 */
public class Orders_Info extends javax.swing.JFrame {

    /**
     * Creates new form Orders_Info
     */
    
    Connection conn = null;    
    TransactionAttributes order;
    TransactionDao transactionDao; 
    
    public Orders_Info() {
        initComponents();
        conn = javaconnect.ConnecrDb();
        transactionDao = new TransactionDao(conn);
        transactionDao.createCustomerTable();
        transactionDao.createAddressTable();
        transactionDao.createOrderTable();
        updateTable();
        
    }
    
    private void readTransactions(File newTranscation){
        try{
            List<TransactionAttributes>  transactions= new ArrayList<TransactionAttributes>();
            
            DocumentBuilderFactory dbFac = DocumentBuilderFactory.newInstance();
            DocumentBuilder dbBuild = dbFac.newDocumentBuilder();
            Document doc = dbBuild.parse(newTranscation);
            doc.getDocumentElement().normalize();
            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
            NodeList nlist = doc.getElementsByTagName("Order");
            
            for(int i = 0; i < nlist.getLength(); i++){
                order = new TransactionAttributes(); //Holds Each Order
                Node node = nlist.item(i);
                System.out.println("---------------------------------------------------");
                if(node.getNodeType() == Node.ELEMENT_NODE){
                    Element mainel = (Element) node;
                    NodeList nl = mainel.getElementsByTagName("ReferenceNum");
                    //System.out.println(nl.item(0).getChildNodes().item(0).getNodeValue());
                    order.setReferenceNum(nl.item(0).getChildNodes().item(0).getNodeValue());
                    
                    nl = mainel.getElementsByTagName("CountryCode");
                    //System.out.println(nl.item(0).getChildNodes().item(0).getNodeValue());
                    order.setCountryCode(nl.item(0).getChildNodes().item(0).getNodeValue());
                    
                    //Get Address of Order
                    //nl = nlist.item(i).getChildNodes();
                    nl = mainel.getElementsByTagName("Address");
                    //nl = nl.item(0).getChildNodes();
                    //for(int m = 0; m < nl.getLength(); m++){
                        Node addr =  nl.item(0);
                        System.out.println("Current Element :" + addr.getNodeName());
                        if(addr.getNodeType() == Node.ELEMENT_NODE){
                            Element el = (Element) addr;
                            
                            NodeList nle = el.getElementsByTagName("FullName");
                            //System.out.println(nle.item(0).getChildNodes().item(0).getNodeValue());
                            order.setAddressName(nle.item(0).getChildNodes().item(0).getNodeValue());
                            
                            nle = el.getElementsByTagName("AddressType");
                            //System.out.println(nle.item(0).getChildNodes().item(0).getNodeValue());
                            order.setAddressType(nle.item(0).getChildNodes().item(0).getNodeValue());
                            
                            nle = el.getElementsByTagName("AddressLine1");
                            //System.out.println(nle.item(0).getChildNodes().item(0).getNodeValue());
                            order.setAddressLine1(nle.item(0).getChildNodes().item(0).getNodeValue());
                            
                            nle = el.getElementsByTagName("AddressLine2");
                            //System.out.println(nle.item(0).getChildNodes().item(0).getNodeValue());
                            order.setAddressLine2(nle.item(0).getChildNodes().item(0).getNodeValue());
                        }
                    
                    //Get Customer details
                     nl = mainel.getElementsByTagName("Customer");
                    //nl = nl.item(0).getChildNodes();
                    //for(int m = 0; m < nl.getLength(); m++){
                        Node cus =  nl.item(0);
                        System.out.println("Current Element :" + cus.getNodeName());
                        if(cus.getNodeType() == Node.ELEMENT_NODE){
                            Element el = (Element) cus;
                            
                            NodeList nle = el.getElementsByTagName("CustomerCode");
                            //System.out.println(nle.item(0).getChildNodes().item(0).getNodeValue());
                            order.setCustomerCode(nle.item(0).getChildNodes().item(0).getNodeValue());
                            
                            nle = el.getElementsByTagName("FirstName");
                            //System.out.println(nle.item(0).getChildNodes().item(0).getNodeValue());
                            order.setCustomerFName(nle.item(0).getChildNodes().item(0).getNodeValue());
                            
                            nle = el.getElementsByTagName("LastName");
                            //System.out.println(nle.item(0).getChildNodes().item(0).getNodeValue());
                            order.setCustomerLName(nle.item(0).getChildNodes().item(0).getNodeValue());
                            
                            nle = el.getElementsByTagName("Phone");
                            //System.out.println(nle.item(0).getChildNodes().item(0).getNodeValue());
                            order.setCustomerPhone(nle.item(0).getChildNodes().item(0).getNodeValue());
                            
                            
                            nle = el.getElementsByTagName("Email");
                            //System.out.println(nle.item(0).getChildNodes().item(0).getNodeValue());
                            order.setCustomerEmail(nle.item(0).getChildNodes().item(0).getNodeValue());
                            
                        }   
                    //Get Orders
                    nl = mainel.getElementsByTagName("OrderLines");
                    Node subOrder = nl.item(0);
                    Element subOrderElement = (Element) subOrder;
                    nl = subOrderElement.getElementsByTagName("OrderLine");
                    
                    for(int m = 0; m < nl.getLength(); m++){
                        Node orders = nl.item(m);
                        if(orders.getNodeType() == Node.ELEMENT_NODE){
                            Element el = (Element) orders;
                            
                            
                            NodeList nle = el.getElementsByTagName("ItemNum");
                            //System.out.println(nle.item(0).getChildNodes().item(0).getNodeValue());
                            String ItemNo = nle.item(0).getChildNodes().item(0).getNodeValue();
                            
                            nle = el.getElementsByTagName("ItemDescription");
                            //System.out.println(nle.item(0).getChildNodes().item(0).getNodeValue());
                            String ItemDesc = nle.item(0).getChildNodes().item(0).getNodeValue();
                            order.setOrders(ItemNo, ItemDesc);
                        } 
                    }
                    //System.out.println(el.getElementsByTagName("ReferenceNum").item(0).getTextContent());
                    transactions.add(order);
                    //break;
                }
                
               /* NodeList newNode = node.getChildNodes();
                for(int m = 0; m < newNode.getLength(); m++){
                    Node innerNode = newNode.item(m);
                    System.out.println("\nCurrent Element :" + innerNode.getNodeName());
                    
                }*/
                
            }
            
            System.out.println("Transaction Read");
            insertData(transactions);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
       
    public void insertData(List<TransactionAttributes>  transactions){
        for(TransactionAttributes trans : transactions){
            transactionDao.insertCustomer(trans.getCustomerFName(), trans.getCustomerLName(), 
                    trans.getCustomerPhone() , trans.getCustomerCode(),trans.getCustomerEmail());
            
            /* AddressID generated with refNum and Cuscode for first appearance of address
            *  If Address is Similar, ignore insert
            */
            try {
                ResultSet rs = transactionDao.verifyAddress(trans.getAddressName(), trans.getAddressType(), trans.getAddressLine1(), trans.getAddressLine2(), trans.getCountryCode());
                if (rs.next()) {
                    trans.setAddressID(rs.getString("AddressID"));
                } else {
                    //Address does not exist in DB
                    trans.setAddressID(trans.getReferenceNum() + trans.getCustomerCode());
                    transactionDao.insertAddress(trans.getAddressID(), trans.getAddressName(), trans.getAddressType(),
                            trans.getAddressLine1(), trans.getAddressLine2(), trans.getCountryCode());
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            
            
            
            for(String key : trans.getOrders().keySet()){
                
                transactionDao.insertOrder(trans.getReferenceNum(), key , 
                        trans.getOrders().get(key), trans.getCustomerCode(), trans.getAddressID());            
            }
        }
        
        System.out.println("Transactions recorded Successfully");
        updateTable();
        
    }

    private void updateTable() {
        Table_Orders.setModel(DbUtils.resultSetToTableModel(transactionDao.seletOrders()));
    }
    
    public void displayCustomerInfo(String CustomerCode){
        try {
            ResultSet rs = transactionDao.selectCustomer(CustomerCode);
            if(rs.next()){
                txtCusCode.setText(rs.getString("Code"));
                txtCusFirstName.setText(rs.getString("FirstName"));
                txtCusLastName.setText(rs.getString("LastName"));
                txtCusPhone.setText(rs.getString("Phone"));
                txtCusEmail.setText(rs.getString("Email"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Orders_Info.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void displayAddressInfo(String AddressID){
           
        try {
            ResultSet rs = transactionDao.selectAddress(AddressID);
            if(rs.next()){
                txtAddrID.setText(rs.getString("AddressID"));
                txtAddrType.setText(rs.getString("AddressType"));
                txtAddrLine1.setText(rs.getString("AddressLine1"));
                txtAddrLine2.setText(rs.getString("AddressLine2"));
                txtAddrCountry.setText(rs.getString("CountryCode"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Orders_Info.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
    /**
     * @define Update changed order changes
     * @param OrderRef
     * @param OldItemNo
     * @param NewItemNo
     * @param ItemDesc 
     */
    public void updateOrders(String OrderRef, String OldItemNo, String NewItemNo, String ItemDesc){
        int ret = transactionDao.updateOrder(OrderRef, OldItemNo, NewItemNo, ItemDesc);
        updateTable();
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnUpload = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtCusCode = new javax.swing.JTextField();
        txtCusFirstName = new javax.swing.JTextField();
        txtCusLastName = new javax.swing.JTextField();
        txtCusPhone = new javax.swing.JTextField();
        txtCusEmail = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnDeleteOrder = new javax.swing.JButton();
        btnUpdateCustomer = new javax.swing.JButton();
        btnUpdateOrder = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        Table_Orders = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        txtAddrID = new javax.swing.JTextField();
        txtAddrType = new javax.swing.JTextField();
        txtAddrLine1 = new javax.swing.JTextField();
        txtAddrLine2 = new javax.swing.JTextField();
        txtAddrCountry = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnUpload.setText("Upload New Transaction");
        btnUpload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUploadActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Customer Info", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 12), new java.awt.Color(204, 0, 51))); // NOI18N

        jLabel1.setText("Code");

        jLabel2.setText("First Name");

        jLabel3.setText("Last Name");

        jLabel4.setText("Phone");

        jLabel5.setText("Email");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jLabel4)
                    .addComponent(jLabel3)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtCusCode, javax.swing.GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txtCusFirstName, javax.swing.GroupLayout.DEFAULT_SIZE, 133, Short.MAX_VALUE)
                        .addGap(1, 1, 1))
                    .addComponent(txtCusLastName)
                    .addComponent(txtCusPhone)
                    .addComponent(txtCusEmail))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtCusCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCusFirstName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCusLastName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCusPhone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCusEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        btnDeleteOrder.setText("Delete Order");
        btnDeleteOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteOrderActionPerformed(evt);
            }
        });

        btnUpdateCustomer.setText("Update Customer");
        btnUpdateCustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateCustomerActionPerformed(evt);
            }
        });

        btnUpdateOrder.setText("Update Order");
        btnUpdateOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateOrderActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnDeleteOrder, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnUpdateOrder, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 211, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(btnUpdateCustomer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(64, Short.MAX_VALUE)
                .addComponent(btnDeleteOrder)
                .addGap(19, 19, 19)
                .addComponent(btnUpdateOrder))
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addGap(16, 16, 16)
                    .addComponent(btnUpdateCustomer)
                    .addContainerGap(99, Short.MAX_VALUE)))
        );

        Table_Orders.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        Table_Orders.setToolTipText("Order Information");
        Table_Orders.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Table_OrdersMouseClicked(evt);
            }
        });
        Table_Orders.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Table_OrdersKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(Table_Orders);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Address Info", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 12), new java.awt.Color(204, 0, 51))); // NOI18N

        jLabel6.setText("Name");

        jLabel7.setText("Type");

        jLabel8.setText("Line 1");

        jLabel9.setText("Line 2");

        jLabel10.setText("Country");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel9)
                    .addComponent(jLabel8))
                .addGap(46, 46, 46)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(txtAddrLine2, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addComponent(txtAddrID, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(31, 31, 31)
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addComponent(txtAddrType, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addComponent(txtAddrLine1, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel10)
                        .addGap(18, 18, 18)
                        .addComponent(txtAddrCountry)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtAddrID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(txtAddrType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtAddrLine1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(txtAddrCountry, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtAddrLine2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnUpload, javax.swing.GroupLayout.DEFAULT_SIZE, 793, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(22, 22, 22)
                                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane1)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnUpload)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(22, 22, 22))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(20, Short.MAX_VALUE))))
        );

        jPanel1.getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnUploadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUploadActionPerformed
        final JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Select new transaction");
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setAcceptAllFileFilterUsed(false);
        FileNameExtensionFilter filter = new FileNameExtensionFilter("xml Files", "xml");
        fc.addChoosableFileFilter(filter);
        
        int returnVal = fc.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION){
            File trans = fc.getSelectedFile();
            //System.out.println(trans.getAbsolutePath());
            readTransactions(trans);
        }
        
    }//GEN-LAST:event_btnUploadActionPerformed

    private void Table_OrdersMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Table_OrdersMouseClicked
        int row = Table_Orders.getSelectedRow();
        String CustomerCode = (Table_Orders.getModel().getValueAt(row, 4).toString());
        String AddressID = (Table_Orders.getModel().getValueAt(row, 3).toString());
        
        //System.out.println("CustomerCode : " + CustomerCode + " Address ID : " + AddressID);
        displayCustomerInfo(CustomerCode);
        displayAddressInfo(AddressID);
        
        
    }//GEN-LAST:event_Table_OrdersMouseClicked

    private void Table_OrdersKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Table_OrdersKeyPressed
        int row = 0;
        
        if(evt.getKeyCode() == KeyEvent.VK_UP){
            row = Table_Orders.getSelectedRow() - 1;
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN){
            row = Table_Orders.getSelectedRow() + 1;
        }
        
        String CustomerCode = (Table_Orders.getModel().getValueAt(row, 4).toString());
        String AddressID = (Table_Orders.getModel().getValueAt(row, 3).toString());
        
        //System.out.println("CustomerCode : " + CustomerCode + " Address ID : " + AddressID);
        displayCustomerInfo(CustomerCode);
        displayAddressInfo(AddressID);
    }//GEN-LAST:event_Table_OrdersKeyPressed

    private void btnUpdateCustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateCustomerActionPerformed
        try{
            String value1 = txtCusFirstName.getText();
            String value2 = txtCusLastName.getText();
            String value3 = txtCusPhone.getText();
            String value4 = txtCusEmail.getText();
            String value5 = txtCusCode.getText();
            
            int ret = transactionDao.updateCustomer(value1, value2, value4, value5, value4);
            
            JOptionPane.showMessageDialog(null,"Updated");
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }          
    }//GEN-LAST:event_btnUpdateCustomerActionPerformed

    private void btnDeleteOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteOrderActionPerformed
        int row = Table_Orders.getSelectedRow();
        String OrderRef = (Table_Orders.getModel().getValueAt(row, 0).toString());
        String itemNo = (Table_Orders.getModel().getValueAt(row, 1).toString());
        
        int p = JOptionPane.showConfirmDialog(null, "Confirm Deletion", "Delete", JOptionPane.YES_NO_OPTION );
        
        if (p == 0){
            int ret = transactionDao.deleteOrder(OrderRef, itemNo);
            
            if(ret == 1)
                JOptionPane.showMessageDialog(null,"Deleted");
            else
                JOptionPane.showMessageDialog(null,"Error");
            
            updateTable();
        }
        
    }//GEN-LAST:event_btnDeleteOrderActionPerformed

    private void btnUpdateOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateOrderActionPerformed
        try {
            int row = Table_Orders.getSelectedRow();
            String OrderRef = (Table_Orders.getModel().getValueAt(row, 0).toString());
            String itemNo = (Table_Orders.getModel().getValueAt(row, 1).toString());
            String itemDesc = (Table_Orders.getModel().getValueAt(row, 2).toString());
            transactionDao.close();
            conn.close();
            OrderEdit nt = new OrderEdit();
            nt.setVariables(/*transactionDao,*/ OrderRef, itemNo, itemDesc);
            nt.setVisible(true);
            //new OrderEdit().setVisible(true);
      
            dispose();
        } catch (SQLException ex) {
            Logger.getLogger(Orders_Info.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnUpdateOrderActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Orders_Info.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Orders_Info.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Orders_Info.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Orders_Info.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Orders_Info().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable Table_Orders;
    private javax.swing.JButton btnDeleteOrder;
    private javax.swing.JButton btnUpdateCustomer;
    private javax.swing.JButton btnUpdateOrder;
    private javax.swing.JButton btnUpload;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField txtAddrCountry;
    private javax.swing.JTextField txtAddrID;
    private javax.swing.JTextField txtAddrLine1;
    private javax.swing.JTextField txtAddrLine2;
    private javax.swing.JTextField txtAddrType;
    private javax.swing.JTextField txtCusCode;
    private javax.swing.JTextField txtCusEmail;
    private javax.swing.JTextField txtCusFirstName;
    private javax.swing.JTextField txtCusLastName;
    private javax.swing.JTextField txtCusPhone;
    // End of variables declaration//GEN-END:variables

    
}
