/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordertrans;


import java.util.HashMap;

/**
 *
 * @author dmm Class for Handling transaction data
 */
public class TransactionAttributes {

    private String ReferenceNum;

    private String AddressID;
    private String CountryCode;
    private String AddressName;
    private String AddressType;
    private String AddressLine1;
    private String AddressLine2;

    private String CustomerCode;
    private String CustomerFName;
    private String CustomerLName;
    private String CustomerPhone;
    private String CustomerEmail;
    
    private String ItemNumber;
    private String ItemDescription;

    private HashMap<String, String> Orders;
    
    
    /**
     * 
     */
    public TransactionAttributes(){    
        Orders = new HashMap<String, String>();
    }

    /**
     * @return the ReferenceNum
     */
    public String getReferenceNum() {
        return ReferenceNum;
    }

    /**
     * @param ReferenceNum the ReferenceNum to set
     */
    public void setReferenceNum(String ReferenceNum) {
        this.ReferenceNum = ReferenceNum;
    }

    /**
     * @return the CountryCode
     */
    public String getCountryCode() {
        return CountryCode;
    }

    /**
     * @param CountryCode the CountryCode to set
     */
    public void setCountryCode(String CountryCode) {
        this.CountryCode = CountryCode;
    }

    /**
     * @return the AddressName
     */
    public String getAddressName() {
        return AddressName;
    }

    /**
     * @param AddressName the AddressName to set
     */
    public void setAddressName(String AddressName) {
        this.AddressName = AddressName;
    }

    /**
     * @return the AddressType
     */
    public String getAddressType() {
        return AddressType;
    }

    /**
     * @param AddressType the AddressType to set
     */
    public void setAddressType(String AddressType) {
        this.AddressType = AddressType;
    }

    /**
     * @return the AddressLine1
     */
    public String getAddressLine1() {
        return AddressLine1;
    }

    /**
     * @param AddressLine1 the AddressLine1 to set
     */
    public void setAddressLine1(String AddressLine1) {
        this.AddressLine1 = AddressLine1;
    }

    /**
     * @return the AddressLine2
     */
    public String getAddressLine2() {
        return AddressLine2;
    }

    /**
     * @param AddressLine2 the AddressLine2 to set
     */
    public void setAddressLine2(String AddressLine2) {
        this.AddressLine2 = AddressLine2;
    }

    /**
     * @return the CustomerCode
     */
    public String getCustomerCode() {
        return CustomerCode;
    }

    /**
     * @param CustomerCode the CustomerCode to set
     */
    public void setCustomerCode(String CustomerCode) {
        this.CustomerCode = CustomerCode;
    }

    /**
     * @return the CustomerFName
     */
    public String getCustomerFName() {
        return CustomerFName;
    }

    /**
     * @param CustomerFName the CustomerFName to set
     */
    public void setCustomerFName(String CustomerFName) {
        this.CustomerFName = CustomerFName;
    }

    /**
     * @return the CustomerLName
     */
    public String getCustomerLName() {
        return CustomerLName;
    }

    /**
     * @param CustomerLName the CustomerLName to set
     */
    public void setCustomerLName(String CustomerLName) {
        this.CustomerLName = CustomerLName;
    }

    /**
     * @return the CustomerPhone
     */
    public String getCustomerPhone() {
        return CustomerPhone;
    }

    /**
     * @param CustomerPhone the CustomerPhone to set
     */
    public void setCustomerPhone(String CustomerPhone) {
        this.CustomerPhone = CustomerPhone;
    }

    /**
     * @return the CustomerEmail
     */
    public String getCustomerEmail() {
        return CustomerEmail;
    }

    /**
     * @param CustomerEmail the CustomerEmail to set
     */
    public void setCustomerEmail(String CustomerEmail) {
        this.CustomerEmail = CustomerEmail;
    }

    /**
     * @return the Orders
     */
    public HashMap<String, String> getOrders() {
        return Orders;
    }

    /**
     * @param Orders the Orders to set
     */
    public void setOrders(/*HashMap<String, String> Orders*/String ItemNo, String ItemDesc) {
        
        this.Orders.put(ItemNo, ItemDesc);
    }

    /**
     * @return the AddressID
     */
    public String getAddressID() {
        return AddressID;
    }

    /**
     * @param AddressID the AddressID to set
     */
    public void setAddressID(String AddressID) {
        this.AddressID = AddressID;
    }

    /**
     * @return the ItemNumber
     */
    public String getItemNumber() {
        return ItemNumber;
    }

    /**
     * @param ItemNumber the ItemNumber to set
     */
    public void setItemNumber(String ItemNumber) {
        this.ItemNumber = ItemNumber;
    }

    /**
     * @return the ItemDescription
     */
    public String getItemDescription() {
        return ItemDescription;
    }

    /**
     * @param ItemDescription the ItemDescription to set
     */
    public void setItemDescription(String ItemDescription) {
        this.ItemDescription = ItemDescription;
    }

}
